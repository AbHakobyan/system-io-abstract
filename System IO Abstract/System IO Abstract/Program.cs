﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System_IO_Abstract.RepasitoryForFile;
using System_IO_Abstract.Repasitory_Models;

namespace System_IO_Abstract
{
    class Program
    {
        static void Main(string[] args)
        {
            var parents = CreateParents(2).ToArray();
            var temp = new Parents_Repasitory();
            temp.Save(parents);
            //List<Parents> parent = temp.ReadAll();
            //foreach (var item in parent)
            //{
            //    Console.WriteLine(item.FullName);
            //}
            //Console.WriteLine(temp.GetLastLine());
            //Console.WriteLine(temp.GetFirstLine());
            temp.DeleteText();
        }

        static IEnumerable<Parents> CreateParents(int count)
        {
            string[] name =
            {
                "Ashot",
                "Vahan",
                "Nelli",
                "Mary"
            };
            string[] surname =
            {
                "Axabekyan",
                "Militosyan",
                "Gevorgyan",
                "Simonyan"
            };

            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                var index = rnd.Next(0, name.Length);
                var index2 = rnd.Next(0, surname.Length);
                yield return new Parents
                {
                    Name = name[index],
                    Surname = surname[index2],
                    Age = (byte)rnd.Next(35, 60),
                    University = (University)rnd.Next(0,4)
                };
            }
        }


    }
}
