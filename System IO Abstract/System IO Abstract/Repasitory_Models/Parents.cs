﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System_IO_Abstract.Repasitory_Models
{
    class Parents
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte Age { get; set; }
        public University University { get; set; }

        public string FullName => $"{Name} {Surname}";

        public override string ToString() => FullName;
    }
}
