﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System_IO_Abstract;
using System_IO_Abstract.Repasitory_Models;


namespace System_IO_Abstract.RepasitoryForFile
{
    class Students_Repasitory : Base_Repasitory<Students>
    {
        public override string Path => "TestDir.txt";

        public override void OnReadFill(string[] data, Students model)
        {
            switch (data[0])
            {
                case nameof(model.Name):
                    model.Name = data[1];
                    break;
                case nameof(model.Surname):
                    model.Surname = data[1];
                    break;
                case nameof(model.Age):
                    if (byte.TryParse(data[1], out byte age))
                        model.Age = age;
                    break;
                case nameof(model.University):
                    if (byte.TryParse(data[1], out byte university))
                        model.University = (University)university;
                    break;

            }
        }

        public override void WriterModel(StreamWriter writer, Students model)
        {
            writer.WriteLine($"{nameof(model.Name)}:{model.Name}");
            writer.WriteLine($"{nameof(model.Surname)}:{model.Surname}");
            writer.WriteLine($"{nameof(model.Age)}:{model.Age}");
            writer.WriteLine($"{nameof(model.University)}:{model.University}");
        }
    }
}
