﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System_IO_Abstract;
using System_IO_Abstract.Repasitory_Models;
using System.IO;

namespace System_IO_Abstract.RepasitoryForFile
{
    public abstract class Base_Repasitory<T> where T : class,new()
    {
        public abstract string Path { get; }

        public int Save(IEnumerable<T> models)
        {
            int count = 0;
            StreamWriter writer = new StreamWriter(Path, true, Encoding.Default);
            foreach (var item in models)
            {
                writer.WriteLine("{");
                WriterModel(writer, item);
                writer.WriteLine("}");

                count++;
            }
            writer.Dispose();

            return count;

        }

        public List<T> ReadAll()
        {
            var item = new List<T>();

            StreamReader reader = new StreamReader(Path, Encoding.Default);

            T temp = null;

            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                switch (line)
                {
                    case "{":
                        temp = new T();
                        break;
                    case "}":
                        item.Add(temp);
                        temp = null;
                        break;
                    default:
                        ReadFill(line, temp);
                        break;
                        
                }
            }
            return item;
        }

        public abstract void WriterModel(StreamWriter writer, T model);
        public abstract void OnReadFill(string[] data, T model);

        public void ReadFill(string line, T model)
        {
            string[] data = line.Split(':');
            if (data.Length < 2)
                return;
            OnReadFill(data, model);
        }

        public void DeleteText()
        {
            StreamWriter writer = new StreamWriter(Path, false); 
        }

        public T GetLastLine()
        {
            string[] lines = File.ReadAllLines(Path);

            T temp = null;

            for (int i = lines.Length - 1; i >= 0; i--)
            {
                string line = lines[i];
                if (line != "{")
                {
                    switch (line)
                    {
                        case "}":
                            temp = new T();
                            break;
                        default:
                            if (line != null)
                            ReadFill(line, temp);
                            break;
                    }
                }
                else
                {
                    break;
                }
            }
            return temp;

        }

        public T GetFirstLine()
        {
            StreamReader reader = new StreamReader(Path);

            T temp = null;

            string line = null;
            while (line != "}" && !reader.EndOfStream)
            {
                line = reader.ReadLine();
                switch (line)
                {
                    case "{":
                        temp = new T();
                        break;
                    default:
                        ReadFill(line, temp);
                        break;
                }
            }

            return temp;
        }
    }
}
